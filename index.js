const express = require('express');
const app = express();

const mongoose = require('mongoose');
const keys = require('./config/keys');

mongoose.connect(keys.mongoURI);

require('./routes/pingRoutes')(app);
require('./routes/dataRoutes')(app);

const PORT = process.env.PORT || 5000;

app.listen(PORT);
console.log('listening on port : ',PORT);
